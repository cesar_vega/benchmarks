<?php

use Phalcon\DI,
    \Phalcon\Db\Adapter\Pdo\Mysql as Connection,
    Phalcon\Mvc\Model\Manager as ModelsManager,
    Phalcon\Mvc\Model\Metadata\Memory as MetaData,
    Phalcon\Mvc\Model;

$di = new DI();

$config = require __DIR__ . '/../config/config.php';

// Setup a connection to wncar_psis
$di->set('db', new Connection($config['database']));

//Setup a connection wncasd_hrs
$config['database']['dbname'] = 'wncasd_hrs';
$di->set('dbHrs', new Connection($config['database']));

//Set a models manager
$di->set('modelsManager', new ModelsManager());

//Use the memory meta-data adapter or other
$di->set('modelsMetadata', new MetaData());

class Staff extends Model
{
    public function getSource()
    {
        return 'SCC_PERDATA_QVW';
    }
    
    public function initialize() 
    {
        $this->hasOne('EMPLID', 'EmployeeFacts', 'EMPLID');
    }
    
    public function getDepartment()
    {
        return $this->EmployeeFacts->getDepartment();
    }
}

class EmployeeFacts extends Model
{
    public function getSource()
    {
        return 'employee_facts';
    }
    
    public function initialize() 
    {
        $this->setConnectionService('dbHrs');
    }
    
    public function getDepartment()
    {
        return $this->hrs_department;
    }
}


$iterations = 50;
$times = array();

for ($i = 0; $i < $iterations; $i++) {
    
    $start = microtime(true);
    $test = Staff::findFirst(1004359069);
    $test->getDepartment();
    $time  = microtime(true) - $start;
    
    echo $time . '<hr/>';
    
    $times[] = $time;
}

echo "Total: " . (array_sum($times) /$iterations) ;