<?php

require 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule;

$capsule->addConnection(array(
    'driver'    => 'mysql',
    'host'      => 'bilbo.scsr.nevada.edu',
    'database'  => 'wncar_psis',
    'username'  => '1004359069',
    'password'  => 'ijMatW!&sf10c',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
));

$capsule->bootEloquent();

class Staff extends Illuminate\Database\Eloquent\Model 
{
    protected $table = 'SCC_PERDATA_QVW';
    
    protected $primaryKey = 'EMPLID';
    
    public function employeeFacts() 
    {
        return $this->hasOne('EmployeeFacts', 'EMPLID');
    }
    
    public function getDepartment()
    {
        return $this->employeeFacts->hrs_department;
    }
}

class EmployeeFacts extends Illuminate\Database\Eloquent\Model
{
    protected $table = 'wncasd_hrs.employee_facts';
    
    protected $primaryKey = 'EMPLID';
    
}

$iterations = 50;
$times = array();

for ($i = 0; $i < $iterations; $i++) {
    
    $start = microtime(true);
    $me = Staff::find(1004359069);
    $me->getDepartment();
    $time  = microtime(true) - $start;
    
    echo $time . '<hr/>';
    
    $times[] = $time;
}

echo "Total: " . (array_sum($times) /$iterations) ;
